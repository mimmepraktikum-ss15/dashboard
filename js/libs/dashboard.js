var Helper = Helper || {};

(function (context) {
    "use strict";
    /* eslint-env browser */

    context.getNodeFromString = function (nodeString) {
        var div = document.createElement("div");
        div.innerHTML = nodeString;
        return div.firstChild;
    };

    context.getNodeFromTemplate = function (templateSelector, data) {
        var templateText = document.querySelector(templateSelector).innerHTML;
        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                templateText = templateText.replace(new RegExp("{{" + key + "}}", "g"), data[key]);
            }
        }
        return context.getNodeFromString(templateText.replace(/(\r\n|\n|\r)/gm, "").trim());
    };

    context.getAttributeFromCSS = function (attribute, selector) {
        var child = "<span class='" + selector.replace(".", "").replace(/\./g, " ") + "' ></span>",
            el = document.createElement("div"),
            target,
            value;
        el.innerHTML = child;
        target = el.firstChild;
        document.body.appendChild(target);
        value = window.getComputedStyle(target)[attribute];
        document.body.removeChild(target);
        return value;
    };

    context.ajax = function (url, callback) {
        var xhr = new XMLHttpRequest();

        function onreadystatechange() {
            if (xhr.readyState < 4) {
                return;
            }

            if (xhr.status !== 200) {
                return;
            }

            if (xhr.readyState === 4) {
                var result = JSON.parse(xhr.responseText);
                callback(result);
            }
        }

        xhr.onreadystatechange = onreadystatechange;
        xhr.open("GET", url, true);
        xhr.send();
    };

    context.makeSquare = function (node) {
        var size = Math.max(node.clientWidth, node.clientHeight);
        node.style.width = node.style.height = size + "px";
    };

    context.stringEndsWith = function (string, suffix) {
        return string.indexOf(suffix, string.length - suffix.length) !== -1;
    };

}(Helper));

var App = App || {};
App.PageController = function (menu, scrollEffect) {
    "use strict";
    /* eslint-env browser */
    /* global Helper, Velocity */

    var velocity = Velocity,
        pages = [];


    function onMenuItemClicked(event) {
        var menuItems = menu.querySelectorAll("a"),
            linkTargetSelector = document.querySelector(event.target.getAttribute("href"));

        for (var i = 0; i < menuItems.length; i++) {
            menuItems[i].classList.remove("active");
        }
        event.target.classList.add("active");
        velocity(linkTargetSelector, "scroll", scrollEffect);
        return false;
    }

    function resizePages() {
        var pageHeight = window.innerHeight;

        for (var i = 0; i < pages.length; i++) {
            pages[i].style.height = pageHeight + "px";
        }
    }

    function addPage(id, title, active) {
        var page = Helper.getNodeFromString("<li><a href='#" + id + "'>" + title + "</a></li>");

        menu.appendChild(page);
        page.addEventListener("click", onMenuItemClicked);
        if (active === true) {
            page.querySelector("a").classList.add("active");
        }
        pages.push(document.querySelector("#" + id));
        resizePages();
    }

    return {
        addPage: addPage,
        resize: resizePages
    };
};

var App = App || {};
App.NewsController = function (news, newsLinks) {
    "use strict";
    /* eslint-env browser */
    /* global Helper */

    var allNews = [],
        allLinks = [],
        carouselTicker,
        carouselSpeed = 4000;

    function selectNews(linkNode) {
        for (var i = 0; i < allLinks.length; i++) {
            allLinks[i].classList.remove("selected");
        }
        linkNode.classList.add("selected");

        for (i = 0; i < allNews.length; i++) {
            allNews[i].classList.add("hidden");
        }

        news.querySelector("#" + linkNode.getAttribute("news-id")).classList.remove("hidden");
    }

    function selectNextNews() {
        var nextNewsPosition = 0;

        for (var i = 0; i < allLinks.length - 1; i++) {
            if (allLinks[i].classList.contains("selected")) {
                nextNewsPosition = i + 1;
            }
        }
        selectNews(allLinks[nextNewsPosition]);
    }

    function initCarousel() {
        carouselTicker = setInterval(selectNextNews, carouselSpeed);
    }

    function onNewsLinkClicked(event) {
        clearInterval(carouselTicker);
        selectNews(event.target);
        setTimeout(initCarousel, carouselSpeed);
    }

    function addLinkForNews(newNews, selected) {
        var link = Helper.getNodeFromString("<li news-id='news-" + newNews.id + "''></li>"),
            linkWidth, linkBorderWidth, linkMargin;

        if (selected === true) {
            link.classList.add("selected");
        }
        link.addEventListener("click", onNewsLinkClicked);
        newsLinks.appendChild(link);
        allLinks.push(link);
        linkWidth = parseInt(window.getComputedStyle(link, null).width.replace("px", ""));
        linkBorderWidth = parseInt(window.getComputedStyle(link, null)["border-width"].replace("px", ""));
        linkMargin = parseInt(window.getComputedStyle(link, null).margin.replace("px", ""));

        newsLinks.style.width = (allNews.length * (linkWidth + 2 * linkMargin + 2 * linkBorderWidth)) + "px";
    }

    function addNews(newNews, visible) {
        var newsElement = Helper.getNodeFromTemplate("#newsTemplate", newNews);

        if (visible === true) {
            var newsElements = news.querySelectorAll(".active");
            for (var i = 0; i < newsElements.length; i++) {
                newsElement[i].classList.add("hidden");
            }
        } else {
            newsElement.classList.add("hidden");
        }
        news.appendChild(newsElement);
        allNews.push(newsElement);
        addLinkForNews(newNews, visible);
    }

    function loadNews(data) {
        var isFirst = true;

        for (var i = 0; i < data.length; i++) {
            addNews(data[i], isFirst);
            isFirst = false;
        }
        initCarousel();
    }

    return {
        loadNews: loadNews
    };
};

var App = App || {};
App.RepositoryController = function (repositories) {
    "use strict";
    /* eslint-env browser */
    /* global Helper, Rickshaw */

    var currentMousePos = {
        x: 0,
        y: 0
    };

    function onMouseMovedInGraph(event) {
        currentMousePos.x = event.pageX;
        currentMousePos.y = event.pageY;
    }

    function addRepository(repository) {
        var users = function (contributors) {
                var result = "";
                contributors.forEach(function (contributor) {
                    result += contributor.name + ", ";
                });
                return result;
            }(repository.contributors),
            colors = ["rgb(114,191,138)", "rgb(191,121,114)", "rgb(250, 173, 114)", "rgb(255, 255, 255)"],
            graphData = [],
            series = [],
            authors = {},
            index = 0,
            log, hour, node, date, color, graph, hoverDetail;

        if (Helper.stringEndsWith(users, ", ")) {
            users = users.replace(new RegExp(", " + "$"), "");
        }
        repository.users = users;
        repository.back = "OK";

        node = Helper.getNodeFromTemplate("#repositoryTemplate", repository);
        repositories.appendChild(node);
        Helper.makeSquare(node);
        Helper.makeSquare(node.querySelector(".flipper"));

        for (var i = repository.logs.length - 1; i >= 0; i--) {
            log = repository.logs[i];
            date = log.commit_date;
            if (date !== undefined) {
                hour = parseInt((new Date(log.commit_date).getTime() / 1000));
                if (series[log.author] === undefined) {
                    series[log.author] = [];
                }
                if (authors[log.author] === undefined) {
                    authors[log.author] = Object.keys(authors).length;
                }
                series[log.author].push({
                    author: log.author,
                    x: hour,
                    y: authors[log.author],
                    log: log.message_body,
                    id: log.commit
                });
            }
        }

        for (var key in series) {
            color = colors[index] || colors[3];
            graphData.push({
                color: color,
                data: series[key]
            });
            index++;
        }

        graph = new Rickshaw.Graph({
            element: node.querySelector(".graph"),
            width: node.querySelector(".graph").clientWidth,
            height: node.querySelector(".graph").clientHeight,
            renderer: "scatterplot",
            series: graphData,
            min: -1,
            padding: {
                top: 0.1,
                bottom: 0.1,
                left: 0.1,
                right: 0.1
            }
        });

        var Hover = Rickshaw.Class.create(Rickshaw.Graph.HoverDetail, {
            render: function (args) {

                args.detail.sort(function (a, b) {
                    return a.order - b.order;
                }).forEach(function (d) {

                    var line = document.createElement('div');
                    line.className = 'line';

                    var swatch = document.createElement('div');
                    swatch.className = 'swatch';
                    swatch.style.backgroundColor = d.series.color;

                    var label = document.createElement('div');
                    label.className = 'label';
                    label.innerHTML = d.name + ": " + d.formattedYValue;

                    line.appendChild(swatch);
                    line.appendChild(label);

                    var dot = document.createElement('div');
                    dot.className = 'dot';
                    dot.style.top = graph.y(d.value.y0 + d.value.y) + 'px';
                    dot.style.borderColor = d.series.color;

                    this.element.appendChild(dot);

                    dot.className = 'dot active';

                    this.show();

                }, this);
            }
        });

        hoverDetail = new Hover({
            graph: graph
        });

        graph.render();

        node.querySelector(".graph").addEventListener("mousemove", onMouseMovedInGraph);
    }

    function loadRepositories(data) {
        for (var i = 0; i < data.length; i++) {
            addRepository(data[i]);
        }
    }

    return {
        loadRepositories: loadRepositories
    };
};

var App = App || {};
App.ScheduleController = function (schedule) {
    "use strict";
    /* eslint-env browser */
    /* global Helper */

    var titleInfo = schedule.querySelector(".info .title"),
        descriptionInfo = schedule.querySelector(".info .description");

    function onMouseLeftEntry() {
        titleInfo.innerHTML = "";
        descriptionInfo.innerHTML = "";
    }

    function onMouseEntersEntry(event) {
        var title = event.target.getAttribute("title"),
            description = event.target.getAttribute("description");

        if (description === "undefined") {
            description = "";
        }

        description = description.replace("[", "<span class='hint'>[").replace("]", "]</span>");
        titleInfo.innerHTML = title;
        descriptionInfo.innerHTML = description;
    }

    function addEntryToDay(day, entry) {
        var node = Helper.getNodeFromTemplate("#dayTemplate", entry);
        node.style.height = entry.duration * 4 + "em";
        node.addEventListener("click", onMouseEntersEntry);
        node.addEventListener("mouseenter", onMouseEntersEntry);
        node.addEventListener("mouseleave", onMouseLeftEntry);
        schedule.querySelector("." + day).appendChild(node);
    }

    function loadSchedule(data) {
        data.monday.forEach(function (entry) {
            addEntryToDay("monday", entry);
        });
        data.tuesday.forEach(function (entry) {
            addEntryToDay("tuesday", entry);
        });
        data.wednesday.forEach(function (entry) {
            addEntryToDay("wednesday", entry);
        });
        data.thursday.forEach(function (entry) {
            addEntryToDay("thursday", entry);
        });
        data.friday.forEach(function (entry) {
            addEntryToDay("friday", entry);
        });
    }

    return {
        loadSchedule: loadSchedule
    };
};

var App = App || {};
App.Dashboard = (function () {
    "use strict";
    /* eslint-env browser, jquery  */
    /* global Helper */

    var pages,
        news,
        schedule,
        repositories;


    function initPageEvents() {
        window.addEventListener("resize", pages.resize);
    }

    function initPages() {
        pages = new App.PageController(document.querySelector("#menu"), {
            duration: 1000,
            easing: "bounce"
        });

        pages.addPage("title-page", "Start", true);
        pages.addPage("schedule-page", "Zeitplan");
        pages.addPage("repository-page", "Repositories");
        pages.addPage("links-page", "Links");
        pages.addPage("about-page", "About");

        initPageEvents();
    }

    function initNews() {
        news = new App.NewsController(document.querySelector("#news"), document.querySelector("#news-links"));
        Helper.ajax("./data/news.json", news.loadNews);
    }

    function initSchedule() {
        schedule = new App.ScheduleController(document.querySelector("#schedule"));
        Helper.ajax("./data/schedule.json", schedule.loadSchedule);
    }

    function initRepositories() {
        repositories = new App.RepositoryController(document.querySelector("#repositories"));
        Helper.ajax("http://132.199.139.24/~baa56852/data/gitstat-server/data/repositories.json", repositories.loadRepositories);
    }

    function init() {
        initPages();
        initNews();
        initSchedule();
        initRepositories();
    }


    return {
        init: init
    };
}());
