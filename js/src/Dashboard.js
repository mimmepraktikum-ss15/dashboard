var App = App || {};
App.Dashboard = (function () {
    "use strict";
    /* eslint-env browser, jquery  */
    /* global Helper */

    var pages,
        news,
        schedule,
        repositories;


    function initPageEvents() {
        window.addEventListener("resize", pages.resize);
    }

    function initPages() {
        pages = new App.PageController(document.querySelector("#menu"), {
            duration: 1000,
            easing: "bounce"
        });

        pages.addPage("title-page", "Start", true);
        pages.addPage("schedule-page", "Zeitplan");
        pages.addPage("repository-page", "Repositories");
        pages.addPage("links-page", "Links");
        pages.addPage("about-page", "About");

        initPageEvents();
    }

    function initNews() {
        news = new App.NewsController(document.querySelector("#news"), document.querySelector("#news-links"));
        Helper.ajax("./data/news.json", news.loadNews);
    }

    function initSchedule() {
        schedule = new App.ScheduleController(document.querySelector("#schedule"));
        Helper.ajax("./data/schedule.json", schedule.loadSchedule);
    }

    function initRepositories() {
        repositories = new App.RepositoryController(document.querySelector("#repositories"));
        Helper.ajax("http://132.199.139.24/~baa56852/data/gitstat-server/data/repositories.json", repositories.loadRepositories);
    }

    function init() {
        initPages();
        initNews();
        initSchedule();
        initRepositories();
    }


    return {
        init: init
    };
}());
