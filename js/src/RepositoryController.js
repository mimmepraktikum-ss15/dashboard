var App = App || {};
App.RepositoryController = function (repositories) {
    "use strict";
    /* eslint-env browser */
    /* global Helper, Rickshaw */

    var currentMousePos = {
        x: 0,
        y: 0
    };

    function onMouseMovedInGraph(event) {
        currentMousePos.x = event.pageX;
        currentMousePos.y = event.pageY;
    }

    function addRepository(repository) {
        var users = function (contributors) {
                var result = "";
                contributors.forEach(function (contributor) {
                    result += contributor.name + ", ";
                });
                return result;
            }(repository.contributors),
            colors = ["rgb(114,191,138)", "rgb(191,121,114)", "rgb(250, 173, 114)", "rgb(255, 255, 255)"],
            graphData = [],
            series = [],
            authors = {},
            index = 0,
            log, hour, node, date, color, graph, hoverDetail;

        if (Helper.stringEndsWith(users, ", ")) {
            users = users.replace(new RegExp(", " + "$"), "");
        }
        repository.users = users;
        repository.back = "OK";

        node = Helper.getNodeFromTemplate("#repositoryTemplate", repository);
        repositories.appendChild(node);
        Helper.makeSquare(node);
        Helper.makeSquare(node.querySelector(".flipper"));

        for (var i = repository.logs.length - 1; i >= 0; i--) {
            log = repository.logs[i];
            date = log.commit_date;
            if (date !== undefined) {
                hour = parseInt((new Date(log.commit_date).getTime() / 1000));
                if (series[log.author] === undefined) {
                    series[log.author] = [];
                }
                if (authors[log.author] === undefined) {
                    authors[log.author] = Object.keys(authors).length;
                }
                series[log.author].push({
                    author: log.author,
                    x: hour,
                    y: authors[log.author],
                    log: log.message_body,
                    id: log.commit
                });
            }
        }

        for (var key in series) {
            color = colors[index] || colors[3];
            graphData.push({
                color: color,
                data: series[key]
            });
            index++;
        }

        graph = new Rickshaw.Graph({
            element: node.querySelector(".graph"),
            width: node.querySelector(".graph").clientWidth,
            height: node.querySelector(".graph").clientHeight,
            renderer: "scatterplot",
            series: graphData,
            min: -1,
            padding: {
                top: 0.1,
                bottom: 0.1,
                left: 0.1,
                right: 0.1
            }
        });


        graph.render();

        node.querySelector(".graph").addEventListener("mousemove", onMouseMovedInGraph);
    }

    function loadRepositories(data) {
        for (var i = 0; i < data.length; i++) {
            addRepository(data[i]);
        }
    }

    return {
        loadRepositories: loadRepositories
    };
};
