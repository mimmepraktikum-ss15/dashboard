var App = App || {};
App.NewsController = function (news, newsLinks) {
    "use strict";
    /* eslint-env browser */
    /* global Helper */

    var allNews = [],
        allLinks = [],
        carouselTicker,
        carouselSpeed = 4000;

    function selectNews(linkNode) {
        for (var i = 0; i < allLinks.length; i++) {
            allLinks[i].classList.remove("selected");
        }
        linkNode.classList.add("selected");

        for (i = 0; i < allNews.length; i++) {
            allNews[i].classList.add("hidden");
        }

        news.querySelector("#" + linkNode.getAttribute("news-id")).classList.remove("hidden");
    }

    function selectNextNews() {
        var nextNewsPosition = 0;

        for (var i = 0; i < allLinks.length - 1; i++) {
            if (allLinks[i].classList.contains("selected")) {
                nextNewsPosition = i + 1;
            }
        }
        selectNews(allLinks[nextNewsPosition]);
    }

    function initCarousel() {
        carouselTicker = setInterval(selectNextNews, carouselSpeed);
    }

    function onNewsLinkClicked(event) {
        clearInterval(carouselTicker);
        selectNews(event.target);
        setTimeout(initCarousel, carouselSpeed);
    }

    function addLinkForNews(newNews, selected) {
        var link = Helper.getNodeFromString("<li news-id='news-" + newNews.id + "''></li>"),
            linkWidth, linkBorderWidth, linkMargin;

        if (selected === true) {
            link.classList.add("selected");
        }
        link.addEventListener("click", onNewsLinkClicked);
        newsLinks.appendChild(link);
        allLinks.push(link);
        linkWidth = parseInt(window.getComputedStyle(link, null).width.replace("px", ""));
        linkBorderWidth = parseInt(window.getComputedStyle(link, null)["border-width"].replace("px", ""));
        linkMargin = parseInt(window.getComputedStyle(link, null).margin.replace("px", ""));

        newsLinks.style.width = (allNews.length * (linkWidth + 2 * linkMargin + 2 * linkBorderWidth)) + "px";
    }

    function addNews(newNews, visible) {
        var newsElement = Helper.getNodeFromTemplate("#newsTemplate", newNews);

        if (visible === true) {
            var newsElements = news.querySelectorAll(".active");
            for (var i = 0; i < newsElements.length; i++) {
                newsElement[i].classList.add("hidden");
            }
        } else {
            newsElement.classList.add("hidden");
        }
        news.appendChild(newsElement);
        allNews.push(newsElement);
        addLinkForNews(newNews, visible);
    }

    function loadNews(data) {
        var isFirst = true;

        for (var i = 0; i < data.length; i++) {
            addNews(data[i], isFirst);
            isFirst = false;
        }
        initCarousel();
    }

    return {
        loadNews: loadNews
    };
};
