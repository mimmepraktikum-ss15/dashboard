var Helper = Helper || {};

(function (context) {
    "use strict";
    /* eslint-env browser */

    context.getNodeFromString = function (nodeString) {
        var div = document.createElement("div");
        div.innerHTML = nodeString;
        return div.firstChild;
    };

    context.getNodeFromTemplate = function (templateSelector, data) {
        var templateText = document.querySelector(templateSelector).innerHTML;
        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                templateText = templateText.replace(new RegExp("{{" + key + "}}", "g"), data[key]);
            }
        }
        return context.getNodeFromString(templateText.replace(/(\r\n|\n|\r)/gm, "").trim());
    };

    context.getAttributeFromCSS = function (attribute, selector) {
        var child = "<span class='" + selector.replace(".", "").replace(/\./g, " ") + "' ></span>",
            el = document.createElement("div"),
            target,
            value;
        el.innerHTML = child;
        target = el.firstChild;
        document.body.appendChild(target);
        value = window.getComputedStyle(target)[attribute];
        document.body.removeChild(target);
        return value;
    };

    context.ajax = function (url, callback) {
        var xhr = new XMLHttpRequest();

        function onreadystatechange() {
            if (xhr.readyState < 4) {
                return;
            }

            if (xhr.status !== 200) {
                return;
            }

            if (xhr.readyState === 4) {
                var result = JSON.parse(xhr.responseText);
                callback(result);
            }
        }

        xhr.onreadystatechange = onreadystatechange;
        xhr.open("GET", url, true);
        xhr.send();
    };

    context.makeSquare = function (node) {
        var size = Math.max(node.clientWidth, node.clientHeight);
        node.style.width = node.style.height = size + "px";
    };

    context.stringEndsWith = function (string, suffix) {
        return string.indexOf(suffix, string.length - suffix.length) !== -1;
    };

}(Helper));
