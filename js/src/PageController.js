var App = App || {};
App.PageController = function (menu, scrollEffect) {
    "use strict";
    /* eslint-env browser */
    /* global Helper, Velocity */

    var velocity = Velocity,
        pages = [];


    function onMenuItemClicked(event) {
        var menuItems = menu.querySelectorAll("a"),
            linkTargetSelector = document.querySelector(event.target.getAttribute("href"));

        for (var i = 0; i < menuItems.length; i++) {
            menuItems[i].classList.remove("active");
        }
        event.target.classList.add("active");
        velocity(linkTargetSelector, "scroll", scrollEffect);
        return false;
    }

    function resizePages() {
        var pageHeight = window.innerHeight;

        for (var i = 0; i < pages.length; i++) {
            pages[i].style.height = pageHeight + "px";
        }
    }

    function addPage(id, title, active) {
        var page = Helper.getNodeFromString("<li><a href='#" + id + "'>" + title + "</a></li>");

        menu.appendChild(page);
        page.addEventListener("click", onMenuItemClicked);
        if (active === true) {
            page.querySelector("a").classList.add("active");
        }
        pages.push(document.querySelector("#" + id));
        resizePages();
    }

    return {
        addPage: addPage,
        resize: resizePages
    };
};
