var App = App || {};
App.ScheduleController = function (schedule) {
    "use strict";
    /* eslint-env browser */
    /* global Helper */

    var titleInfo = schedule.querySelector(".info .title"),
        descriptionInfo = schedule.querySelector(".info .description");

    function onMouseLeftEntry() {
        titleInfo.innerHTML = "";
        descriptionInfo.innerHTML = "";
    }

    function onMouseEntersEntry(event) {
        var title = event.target.getAttribute("title"),
            description = event.target.getAttribute("description");

        if (description === "undefined") {
            description = "";
        }

        description = description.replace("[", "<span class='hint'>[").replace("]", "]</span>");
        titleInfo.innerHTML = title;
        descriptionInfo.innerHTML = description;
    }

    function addEntryToDay(day, entry) {
        var node = Helper.getNodeFromTemplate("#dayTemplate", entry);
        node.style.height = entry.duration * 4 + "em";
        node.addEventListener("click", onMouseEntersEntry);
        node.addEventListener("mouseenter", onMouseEntersEntry);
        node.addEventListener("mouseleave", onMouseLeftEntry);
        schedule.querySelector("." + day).appendChild(node);
    }

    function loadSchedule(data) {
        data.monday.forEach(function (entry) {
            addEntryToDay("monday", entry);
        });
        data.tuesday.forEach(function (entry) {
            addEntryToDay("tuesday", entry);
        });
        data.wednesday.forEach(function (entry) {
            addEntryToDay("wednesday", entry);
        });
        data.thursday.forEach(function (entry) {
            addEntryToDay("thursday", entry);
        });
        data.friday.forEach(function (entry) {
            addEntryToDay("friday", entry);
        });
    }

    return {
        loadSchedule: loadSchedule
    };
};
