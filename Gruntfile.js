(function () {
    "use strict";
    /* global module */
    module.exports = function (grunt) {
        grunt.initConfig({
            pkg: grunt.file.readJSON("package.json"),

            concat: {
                options: {
                    separator: grunt.util.linefeed
                },
                dist: {
                    src: ["src/js/JSBouncer_Utilities.js", "src/js/JSBouncer_Enums.js", "src/js/JSBouncer_Prototypes.js", "src/js/JSBouncer_Connector.js", "src/js/JSBouncer_Map.js", "src/js/JSBouncer_Bouncer.js", "src/js/JSBouncer_Context.js", "src/js/JSBouncer_Main.js"],
                    dest: "dist/bouncer.js"
                }
            }
        });
        // dependencies
        grunt.loadNpmTasks("grunt-contrib-concat");
        // tasks
        grunt.registerTask("default", ["concat"]);
    };
}());
